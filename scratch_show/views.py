from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.core.exceptions import ValidationError

from . import forms, models


class IndexView(TemplateView):
    template_name = "scratch_show/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        today = timezone.now().date()
        context['current_event'] = models.Event.objects.filter(date=today, accept_projects=True).first()
        context['past_events'] = models.Event.objects.exclude(date=today, accept_projects=True).order_by('-date')
        return context


class ScratchProjectAddView(CreateView):
    model = models.ScratchProject
    fields = ['name', 'author_name', 'project_url']

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(models.Event, date=timezone.now().date(), accept_projects=True)
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.event = self.event
        return super().form_valid(form)


class ScratchProjectDetailView(DetailView):
    model = models.ScratchProject

class EventDetailView(DetailView):
    model = models.Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['projects'] = models.ScratchProject.objects.filter(event=context['object'].pk)
        return context
