from datetime import date

from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator

class Event(models.Model):
    name = models.CharField("Nom de l'évènement", max_length=128)
    date = models.DateField(default=date.today)
    accept_projects = models.BooleanField(default=True)


class ScratchProject(models.Model):
    name = models.CharField('Nom du projet', max_length=128)
    author_name = models.CharField('Auteur', max_length=128)
    project_url = models.URLField('Lien (url) du projet Scratch', validators=[RegexValidator(regex=r'https?://scratch\.mit\.edu/projects/([0-9]+)/?')])
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def get_absolute_url(self):
        return reverse('scratch_show:project-detail', kwargs={'pk' : self.pk})
