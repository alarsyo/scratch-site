from django.urls import path
from . import views

app_name = 'scratch_show'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('add', views.ScratchProjectAddView.as_view(), name='project-add'),
    path('project/<int:pk>/', views.ScratchProjectDetailView.as_view(), name='project-detail'),
    path('event/<int:pk>/', views.EventDetailView.as_view(), name='event-detail'),
]
