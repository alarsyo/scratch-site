from django.apps import AppConfig


class ScratchShowConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'scratch_show'
